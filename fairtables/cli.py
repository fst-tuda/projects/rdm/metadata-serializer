import json
from pathlib import Path
from typing import Any, Dict, List, Optional

from fairtables.assigner import Assigner
from fairtables.objects import DataObject


class CLI:
    def __init__(
        self,
        output_path: str,
        data_list: Optional[List[str]] = None,
        metadata_list: Optional[List[str]] = None,
    ) -> None:
        self.assigner = Assigner()
        self.metadata = {}
        self.output_path = output_path

        if data_list:
            self.data_list = [Path(path) for path in data_list]
        else:
            self.data_list = []
        if metadata_list:
            self.metadata_list = [Path(path) for path in metadata_list]
        else:
            self.metadata_list = []

    def read_csv_files(self):
        for file in self.data_list:
            dataset_id = self.metadata.get(file.name, {}).get("@id")
            colum_ids = self._get_colum_ids(self.metadata.get(file.name, {}))
            complex_colum = self._get_complex_colum(self.metadata.get(file.name))
            id = self.assigner.read_data_from_csv(
                file,
                id=dataset_id,
                column_id_list=colum_ids,
                complex_columns=complex_colum,
            )

            if self._has_metadata(file):
                self.metadata[file.name]["@id"] = id

        for key, value in self.metadata.items():
            self.metadata[key] = self._to_metadata_object(value)
        for value in self.metadata.values():
            self._connect_data_metadata(value)

    def _get_colum_ids(self, file_metadata: dict[Any, Any]):
        ids = []
        for value in file_metadata.values():
            if isinstance(value, dict):
                ids.append(value.get("@id"))
        return ids

    def _get_complex_colum(self, metadata: Optional[dict[Any, Any]]) -> List[str]:
        complex = []
        if not metadata:
            return complex
        for key, value in metadata.items():
            if isinstance(value, dict):
                for colum_key in value:
                    if colum_key == "@complex" and value.get("@complex") == "True":
                        complex.append(key)
                        break
        return complex

    def _has_metadata(self, file_path):
        return self.metadata and file_path.name in self.metadata

    def to_hdf5(self):
        self.assigner.to_hdf5(self.output_path)

    def read_json_files(self):
        # TODO: Always read json first!
        for path in self.metadata_list:
            with open(path, "r") as f:
                self.metadata.update(json.loads(f.read()))
        self._add_all_ref()

    def _add_all_ref(self):
        for meta_key, meta_value in self.metadata.items():
            if meta_key[0] != "@":
                self._add_dict_ref(meta_value)

    def _add_dict_ref(self, metadata: dict):
        if metadata.get("@link"):
            ref_dict = self._get_ref(metadata["@link"])
            metadata.pop("@link")
            metadata.update(ref_dict)
        for meta_value in metadata.values():
            if isinstance(meta_value, dict):
                self._add_dict_ref(meta_value)

    def _get_ref(self, label: str):
        ref_metadata = self.metadata.get(label)
        if not ref_metadata:
            raise KeyError("{} not found.".format(label))
        if not isinstance(ref_metadata, dict):
            raise ValueError("{} must link to a dict.".format(label))

        if "@link" in ref_metadata:
            recursive_ref = self._get_ref(ref_metadata["@link"])
            ref_metadata.pop("@link")
            ref_metadata.update(recursive_ref)

        return ref_metadata

    def _to_metadata_object(self, dictionary: Dict[Any, Any]):
        # TODO: IDs in json vorgeben, und zu lang
        file_metadata = {}
        if "@id" not in dictionary:
            return dictionary
        metadata_mapper = {"@id": dictionary["@id"], "@metadata": None}

        for key, value in dictionary.items():
            if key[0] == "@":
                continue
            if isinstance(value, dict):
                metadata_no_prefix = self._remove_prefix_key(value)
                metadata_id = self.assigner.read_metadata_from_dict(metadata_no_prefix)
                data_objects = CLI.get_data_object_per_name(
                    self.assigner.data_objects[dictionary["@id"]], key
                )
                if len(data_objects) != 1:
                    raise ValueError(
                        "Internal error, please check the keys of the json file."
                    )
                data_id = data_objects[0].id
                metadata_mapper[data_id] = metadata_id
            else:
                file_metadata[key] = value

        file_metadata_id = self.assigner.read_metadata_from_dict(file_metadata)
        metadata_mapper["@metadata"] = file_metadata_id
        return metadata_mapper

    def _remove_prefix_key(self, metadata: dict[str, Any]):
        meta_copy = dict(metadata)
        for i in metadata:
            if i[0] == "@":
                meta_copy.pop(i)
        return meta_copy

    def _connect_data_metadata(self, metadata_objects: dict[Any, Any]):
        if "@id" not in metadata_objects:
            return
        for key, value in metadata_objects.items():
            if key == "@metadata":
                continue
            if key == "@id" and metadata_objects["@metadata"] is not None:
                self.assigner.assign_metadata(metadata_objects["@metadata"], value)
            else:
                self.assigner.assign_metadata(value, key)

    @staticmethod
    def get_data_object_per_name(
        data_object: DataObject, name: str
    ) -> List[DataObject]:
        data_objects = []
        for object in data_object.data:
            if isinstance(object, DataObject) and object.name == name:
                data_objects.append(object)
        return data_objects
