"""FAIRtables"""

__author__ = ["Michaela Leštáková", "Ning Xia"]
__email__ = ["michaela.lestakova@tu-darmstadt.de", "ning.xia@tu-darmstadt.de"]
__version__ = "0.0.1"
