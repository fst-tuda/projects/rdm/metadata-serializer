import json
import uuid
from typing import Any, Dict, List, Optional, Union

import h5py as h5
import numpy as np
import pandas as pd

from fairtables.objects import DataObject, MetadataObject


class Assigner:
    def __init__(self) -> None:
        self.data_objects = {}  # TODO ids can not be identical for more than 1 object
        self.metadata_objects = {}
        self._group_id_list = []
        self.file_metadata = None
        pass

    def read_data_from_csv(
        self,
        filepath: str,
        id: str = None,
        name: str = None,
        column_id_list: List = None,
        complex_columns: List = None,
        **pd_kwargs,
    ) -> str:  # TODO ids in column_id_list can not be identical for more than 1 object
        """Reads data to be serialized from a CSV file and stores them as DataObjects.

        Args:
            filepath (str): path to the CSV file
            id (str, optional): A (unique) ID for the file. Defaults to None.
            name (str, optional): Name of the dataset. Defaults to None.
            column_id_list (List, optional): List of (unique) IDs to add to each \
                column. Defaults to None.
            complex_columns (List, optional): List of columns that contain \
                complex numbers. Defaults to [].

        Returns:
            str: ID of the DataObject
        """
        if complex_columns is None:
            complex_columns = []

        data_df = pd.read_csv(
            filepath,
            converters={
                c: lambda s: complex(s.replace("i", "j")) for c in complex_columns
            },
            **pd_kwargs,
        )

        if id is None:
            id = str(uuid.uuid1())
        if name is None:
            name = filepath

        do = DataObject(id, name=name)
        do.from_df(data_df, column_id_list)
        self._group_id_list.append(do.id)

        self.data_objects.update(self._get_all_data_object(do))

        return do.id

    def _get_all_data_object(
        self, data_object: DataObject, data_objects: Dict[str, DataObject] = None
    ):
        if data_objects is None:
            data_objects = {}
        if isinstance(data_object.data, List):
            for object in data_object.data:
                if isinstance(object, DataObject):
                    data_objects = self._get_all_data_object(object, data_objects)

        if (
            data_object.id in data_objects
            and data_object != data_objects[data_object.id]
        ):
            raise ValueError("ID can not be identical.")

        data_objects[data_object.id] = data_object
        return data_objects

    def read_metadata_from_json(
        self, metadata_filepath: str, id: Optional[str] = None
    ) -> str:
        """Reads metadata from a JSON file and stores them as a MetadataObject.

        Args:
            metadata_filepath (str): Path to JSON file containing metadata
            id (str, optional): A (unique) ID to assign. Defaults to None.

        Returns:
            str: ID of the MetadataObject
        """
        mo = self._get_metadata_object_from_json(metadata_filepath, id)
        self.metadata_objects[mo.id] = mo
        return mo.id

    def read_metadata_from_dict(
        self, metadata: Dict[Any, Any], id: Optional[str] = None
    ) -> str:
        """Reads metadata from a dictionary and stores them as a MetadataObject.

        Args:
            metadata (Dict[Any, Any]): A dictionary containing metadata
            id (Optional[str], optional): A (unique) ID to assign. Defaults to None.

        Returns:
            str: ID of the MetadataObject
        """
        mo = self._get_metadata_object_from_dict(metadata, id)
        self.metadata_objects[mo.id] = mo
        return mo.id

    def _get_metadata_object_from_json(
        self, metadata_filepath: str, id: Optional[str]
    ) -> MetadataObject:
        with open(metadata_filepath, "r") as f:
            metadata_dict = json.loads(f.read())
        mo = self._get_metadata_object_from_dict(metadata_dict, id)
        return mo

    def _get_metadata_object_from_dict(
        self, metadata: Dict[Any, Any], id: Optional[str]
    ) -> MetadataObject:
        if id is None:
            id = str(uuid.uuid1())

        mo = MetadataObject(id=id)
        mo.metadata = metadata
        return mo

    def assign_metadata(
        self,
        metadata_object_id: str,
        data_object_id: str,
        column: str = None,  # TODO
    ):
        """Assigns a metadata object to a data object using their IDs. Both objects \
            must have been previously stored on Assigner using the corresponding \
                functions.

        Args:
            metadata_object_id (str): ID of the metadata object
            data_object_id (str): ID of the data object
            column (str, optional): (Not Implemented). Defaults to None.
        """
        mo = self.metadata_objects[metadata_object_id]
        self.data_objects[data_object_id].assign_metadata_object(mo)
        mo.assign_data_object(self.data_objects[data_object_id])

    def remove_objects(self, object_ids: List) -> None:
        for id in object_ids:
            try:
                self.data_objects.pop(id)
            except KeyError:
                try:
                    self.metadata_objects.pop(id)
                except KeyError:
                    print("Object with id {} not found")

    def to_hdf5(
        self, path: str, index_by: str = "id", file_metadata_path: Optional[str] = None
    ) -> str:
        """Exports all objects on Assigner to a HDF5 file.

        Args:
            path (str): Path to the HDF5 object
            index_by (str, optional): Whether to index groups by "id" or "name". \
                Other values not allowed. Defaults to "id".

        Raises:
            ValueError: Data object name cannot be None if index_by is set to 'name'.

        Returns:
            str: path to the created HDF5 file
        """
        with h5.File(path, "w") as f:
            if file_metadata_path is not None:
                self._read_file_metadata(file_metadata_path)
                f.attrs.update(self.file_metadata.metadata)

            for group_id in self._group_id_list:
                if index_by == "id":
                    group = f.create_group(group_id)
                elif index_by == "name":
                    if self.data_objects[group_id].name is not None:
                        group = f.create_group(self.data_objects[group_id].name)
                    else:
                        raise ValueError(
                            "Data object name cannot be None if index_by is \
                                set to 'name'"
                        )

                group.attrs["id"] = group_id

                for mo in self.data_objects[group_id].metadata_objects:
                    group.attrs.update(mo.metadata)
                for do in self.data_objects[group_id].data:
                    self._dump_data_object(group, do)
        return path

    def _read_file_metadata(self, metadata_filepath: str):
        self.file_metadata = self._get_metadata_object_from_json(
            metadata_filepath, None
        )

    def _dump_data_object(
        self, h5_object: Union[h5.File, h5.Group], data_object: DataObject
    ):
        if all([not isinstance(i, DataObject) for i in data_object.data]):
            self._to_hdf5_object(h5_object, data_object, True)
        else:
            group = self._to_hdf5_object(h5_object, data_object, False)
            for i in data_object.data:
                self._dump_data_object(group, i)

        return h5_object

    # TODO: what will be happen if there are same names in the same HDF5 Group?
    def _to_hdf5_object(
        self,
        h5_object: Union[h5.Group, h5.Dataset],
        data_object: DataObject,
        create_dataset: bool,
    ) -> Union[h5.Group, h5.Dataset]:
        if create_dataset:
            if np.array(data_object.data).dtype != complex:
                h5_obj = h5_object.create_dataset(
                    data_object.name, data=data_object.data
                )
            else:
                h5_obj = h5_object.create_group(data_object.name)
                h5_obj.create_dataset(
                    "{}_real".format(data_object.name),
                    data=np.array(data_object.data).real,
                )
                h5_obj.create_dataset(
                    "{}_imag".format(data_object.name),
                    data=np.array(data_object.data).imag,
                )
        else:
            h5_obj = h5_object.create_group(data_object.name)

        h5_obj.attrs["id"] = data_object.id
        # TODO: dump MetadataObject
        if len(data_object.metadata_objects) > 0:
            for mo in data_object.metadata_objects:
                h5_obj.attrs.update(mo.metadata)
        return h5_obj
