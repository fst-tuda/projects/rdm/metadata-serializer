import uuid
from typing import Iterable, List, Union

import pandas as pd


class DataObject:
    def __init__(self, id: str, data=None, name: str = None) -> None:
        self.id: str = id
        self.name: str = name
        self.data = data
        # TODO: should be implemented using @property & setter
        self.metadata_objects: List = []

    def assign_metadata_object(
        self, metadata_object, overwrite: bool = False
    ) -> None:  # TODO implement overwrite
        if isinstance(metadata_object, MetadataObject):
            self.metadata_objects.append(metadata_object)
        else:
            raise TypeError("Metadata must be of type MetadataObject")

    def remove_metadata_object(self):
        raise NotImplementedError()  # TODO

    def from_df(self, data_df: pd.DataFrame, column_id_list: List = None) -> None:
        self.data = []
        # TODO: dict muss fuer column_id_list angewendet werden.
        for i, column in enumerate(data_df.columns):
            if (
                column_id_list is None
                or i >= len(column_id_list)
                or column_id_list[i] is None
            ):
                id = str(uuid.uuid1())
            else:
                id = column_id_list[i]
            c = DataObject(id=id, name=column, data=list(data_df[column]))
            self.data.append(c)


class MetadataObject:
    def __init__(
        self,
        id: str,
        data_objects: Union[DataObject, Iterable[DataObject], None] = None,
        metadata=None,  # TODO: should be implemented using @property & setter
    ) -> None:
        self.id: str = id
        self.data_objects: List = []
        self.metadata = metadata

        if data_objects:
            self.assign_data_object(data_objects)

    def assign_data_object(
        self,
        data_object: DataObject,
        overwrite: bool = False,  # TODO implement overwrite
    ):
        if isinstance(data_object, DataObject):
            self.data_objects.append(data_object)
        else:
            raise TypeError("Data object must be of type DataObject")

    def remove_data_object(self):
        raise NotImplementedError()  # TODO
