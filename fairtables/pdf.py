import os
from typing import Tuple

import h5py

from fairtables.objects import DataObject


class PDF:
    def __init__(self, pdf_file: str) -> None:
        self.pdf_path = pdf_file
        self.data_object = None
        with open(self.pdf_path, "rb") as f:
            self.pdf_data = f.read()

    def data_object(self) -> DataObject:
        if self.data_object is None:
            self.data_object = DataObject(
                id="pdf_{}".format(self.pdf_path), data=self.pdf_data
            )
        return self.data_object

    def add_to_hdf5(self, hdf_file: str, overwrite: bool = False) -> None:
        with h5py.File(hdf_file, "a") as hdf:
            # TODO: Vielleicht kann man doch data_type dynamisch anpassen.
            data_type = "S{}".format(len(self.pdf_data))
            # TODO: Besserer Name?
            dataset_name = "pdf_{}".format(self.pdf_path.replace("/", "-"))

            # TODO: DataObject soll (vielleicht) in der Lage sein, mit pdf umzugehen
            try:
                hdf.create_dataset(
                    dataset_name,
                    dtype=data_type,
                    data=self.pdf_data,
                )
                hdf[dataset_name].attrs["file_name"] = os.path.basename(self.pdf_path)
                # TODO: für automatische Erkennung
                hdf[dataset_name].attrs["format"] = ".pdf"
                # TODO: unschön
                hdf[dataset_name].attrs["software"] = (
                    "https://git.rwth-aachen.de/fst-tuda/projects/rdm/metadata-serializer"  # noqa: E501
                )
            except ValueError:
                if not overwrite:
                    print(
                        "Dataset already exist, set overwrite to True to overwrite the dataset {}".format(  # noqa: E501
                            dataset_name
                        )
                    )
                else:
                    hdf[dataset_name][()] = self.pdf_data

    @classmethod
    def as_dataobject(cls, pdf_file: str) -> Tuple[DataObject, "PDF"]:
        pdf = cls(pdf_file)
        return pdf.data_object(), pdf

    @classmethod
    def as_hdf5(cls, pdf_file: str, hdf_file: str, overwrite: bool = False) -> "PDF":
        pdf = cls(pdf_file)
        pdf.add_to_hdf5(hdf_file, overwrite)
        return pdf

    @staticmethod
    def read_pdf_from_hdf5(hdf_file: str, pdf_out: str) -> None:
        pdf_path = PDF.get_all_pdf_dataset_in_root(hdf_file)
        for i in pdf_path:
            PDF.read_one_pdf_from_hdf5(hdf_file, i, pdf_out)

    @staticmethod
    def read_one_pdf_from_hdf5(hdf_file: str, pdf_path: str, pdf_out: str) -> None:
        with h5py.File(hdf_file, "r") as hdf:
            # TODO: momentan muss pdf als Skalar gespeichert werden.
            pdf_data = hdf[pdf_path][()]
            pdf_name = hdf[pdf_path].attrs.get("file_name")

        out_path = os.path.join(pdf_out, pdf_name)
        # TODO: wenn eine Datei mit gleichen Name existiert.
        with open(out_path, "wb") as f:
            f.write(pdf_data)

    @staticmethod
    def get_all_pdf_dataset_in_root(hdf_file: str) -> None:
        hdf_path = []
        with h5py.File(hdf_file, "r") as hdf:
            for i in hdf:
                if hdf[i].attrs.get("format") == ".pdf":
                    hdf_path.append(i)
        return hdf_path
