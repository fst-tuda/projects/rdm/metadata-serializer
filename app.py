#!/usr/bin/env python3
import argparse

from fairtables.cli import CLI


def main():
    parser = argparse.ArgumentParser(prog="fairtables", description="FAIRTables CLI")

    parser.add_argument(
        "-f",
        "--file",
        action="extend",
        nargs="*",
        help="CSV files that should be saved into HDF5.",
        required=False,
    )
    parser.add_argument(
        "-m",
        "--metadata",
        action="extend",
        nargs="*",
        help="JSON files containing metadata.",
        required=False,
    )
    parser.add_argument(
        "-o",
        "--output",
        nargs="?",
        help="Output path of the HDF5 file.",
        required=False,
    )

    args = parser.parse_args()

    if not any(vars(args).values()):
        parser.print_help()
        return

    if args.output:
        output_path = args.output
        if output_path[-3:] != ".h5" and output_path[-5:] != ".hdf5":
            output_path = output_path + ".hdf5"

        cli = CLI(output_path, args.file, args.metadata)
        cli.read_json_files()
        cli.read_csv_files()
        cli.to_hdf5()
    else:
        parser.error("Use the -o or --output option to specify the output path.")


if __name__ == "__main__":
    main()
