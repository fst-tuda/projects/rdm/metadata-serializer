from fairtables import objects


def test_add_metadata_to_data_object():
    data_obj = objects.DataObject("d1")
    metadata_obj = objects.MetadataObject("m1")
    data_obj.assign_metadata_object(metadata_obj)

    assert data_obj.metadata_objects[0].id == "m1"


def test_add_data_to_metadata_object():
    data_obj = objects.DataObject("d1")
    metadata_obj = objects.MetadataObject("m1")
    metadata_obj.assign_data_object(data_obj)

    assert metadata_obj.data_objects[0].id == "d1"
