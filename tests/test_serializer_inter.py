import uuid

import pytest

from fairtables.assigner import Assigner
from fairtables.objects import DataObject


def test_get_all_data_object():
    data_object_big_brother = DataObject(
        str(uuid.uuid1()),
        [
            DataObject(
                str(uuid.uuid1()),
                [
                    DataObject(str(uuid.uuid1())),
                    [1, 2, 3, 4],
                    DataObject(str(uuid.uuid1())),
                    [4, 6, 4, 2],
                    DataObject(str(uuid.uuid1())),
                    [1, 3, 2, 5],
                ],
            ),
            DataObject(
                str(uuid.uuid1()),
                [
                    DataObject(str(uuid.uuid1())),
                    [12, 2, 3, 9],
                    DataObject(str(uuid.uuid1())),
                    [4, 6, 4, 42],
                ],
            ),
            DataObject(
                str(uuid.uuid1()),
                [3, 0, 5, 4],
            ),
        ],
    )
    serialzer = Assigner()
    result = serialzer._get_all_data_object(data_object_big_brother)

    assert len(result) == 9
    for i in result:
        assert i == result[i].id


def test_get_all_data_object_same_id():
    data_object_little_brother = DataObject("123", [DataObject("123")])

    assigner = Assigner()
    with pytest.raises(ValueError):
        assigner._get_all_data_object(data_object_little_brother)

    data_object_same_brother = DataObject("42")
    assigner = Assigner()
    result = assigner._get_all_data_object(
        data_object_same_brother, {"42": data_object_same_brother}
    )
    assert len(result) == 1
