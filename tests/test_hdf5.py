import h5py as h5

from fairtables.assigner import Assigner


def assigner_from_csv():
    s = Assigner()
    s.read_data_from_csv(
        "tests/data/test_data_1.CSV",
        column_id_list=[
            "time_1",
            "pressure_1",
            "temperature_1",
        ],
        id="unique_id_1",
        delimiter=";",
    )
    return s


def test_to_hdf5():
    path = "tests/out/test_result.h5"
    s = assigner_from_csv()
    s.to_hdf5(path)

    with h5.File(path) as f:
        groups = list(f.keys())
        assert len(groups) == 1
        assert groups[0] == "unique_id_1"
        assert f["unique_id_1"].attrs["id"] == "unique_id_1"

        subgroups = list(f["unique_id_1"].keys())
        assert len(subgroups) == 3
        assert "time" in subgroups
        assert "pressure" in subgroups
        assert f["unique_id_1"]["time"].attrs["id"] == "time_1"
        assert f["unique_id_1"]["pressure"].attrs["id"] == "pressure_1"
