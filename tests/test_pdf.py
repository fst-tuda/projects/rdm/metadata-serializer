from fairtables.pdf import PDF


def test_save_pdf_in_hdf5():
    h5_out_path = "tests/out/hdf_pdf.h5"
    pdf_path = "tests/data/tuda_logo.pdf"
    PDF.as_hdf5(pdf_path, h5_out_path)
    pdf_path = "tests/data/tuda_logo_copy.pdf"
    PDF.as_hdf5(pdf_path, h5_out_path)


def test_read_pdf_from_hdf5():
    h5_out_path = "tests/out/hdf_pdf.h5"
    pdf_path = "tests/data/tuda_logo.pdf"
    PDF.as_hdf5(pdf_path, h5_out_path)
    pdf_path = "tests/data/tuda_logo_copy.pdf"
    PDF.as_hdf5(pdf_path, h5_out_path)

    PDF.read_pdf_from_hdf5(
        h5_out_path, "tests/out"
    )
