from fairtables.assigner import Assigner


def test_main():
    a = Assigner()
    a.read_data_from_csv(
        "tests/data/Nusselt_data.csv",
        column_id_list=["Pe_1", "Nu_1", "Nu_StdUnc_1", "Measurement_UUID_1"],
        complex_columns=["Nu", "Nu_StdUnc"],
        delimiter=",",
    )
    a.read_metadata_from_json("tests/data/metadata_nusselt.json", id="Nu_metadata")
    a.assign_metadata("Nu_metadata", "Nu_1")
    a.data_objects["Nu_1"]

    a.to_hdf5(
        "tests/out/test_main.hdf5", file_metadata_path="tests/data/file_meta.json"
    )
