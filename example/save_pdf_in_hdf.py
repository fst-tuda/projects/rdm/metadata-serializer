from fairtables.pdf import PDF


def main():
    # Pfad zur HDF5
    h5_out_path = "hdf_pdf.h5"
    # Pfade zur PDF-Dateien
    pdf_path = [
        "tests/data/tuda_logo.pdf",
    ]

    for i in pdf_path:
        PDF.as_hdf5(i, h5_out_path, overwrite=False)


if __name__ == "__main__":
    main()
