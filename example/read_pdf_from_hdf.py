from fairtables.pdf import PDF


def main():
    # Pfad zur HDF5
    path_hdf = "hdf_pdf.h5"

    PDF.read_pdf_from_hdf5(path_hdf, "./")


if __name__ == "__main__":
    main()
