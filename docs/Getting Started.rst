Getting Started
===============

FAIRtables is a tool that helps you describe tabular data with metadata by linking your **data objects** to **metadata objects**.

**Data objects** represent the tabular data, such as measurement data, simulation results etc., stored in a .CSV format.

Below, you can find an example of tabular data from a .CSV file suitable for FAIRtables.

.. csv-table :: Example table
   :file: test_nu_pe.csv
   :widths: 30, 30, 30
   :header-rows: 1

FAIRtables recognizes the following **data objects** of a table:

* the *table* itself
* the table *columns*

Each of the **data objects** can be described by metadata by assigning a **metadata object** to it.

To each of the data objects, FAIRtables can assign key-value pairs of metadata, stored in JSON files referred to as **metadata objects**.

A metadata object describing the table from the example above can look, for example, like this:

.. literalinclude :: metadata_table.json
   :language: JSON

A metadata object can be created for each of the table columns, for example:

.. literalinclude :: metadata_nusselt.json
   :language: JSON

.. literalinclude :: metadata_peclet.json
   :language: JSON


Linking data objects and metadata objects with FAIRtables
---------------------------------------------------------
First, initialize the `Serializer` class:

..  code-block :: python

    from fairtables.assigner import Assigner
    a = Assigner()

To read the data from a CSV file, use the function `read_data_from_csv`
and specify `column_id_list` (unique IDs for each column) as well as for the table itself (id):

.. code-block :: python

   a.read_data_from_csv("test_nu_pe.csv",
                        column_id_list=["uid_001_pe_data", "uid_002_nu_data", "uid_003_mid_data"],
                        id="uid_000_table_data",
                        delimiter=",")

The above method uses `pandas.read_csv()` and can pass keyword arguments to it (such as delimiter in the above example).

To read in a metadata file and assign a unique ID to it, use the method `read_metadata_from_json`.

.. code-block :: python

   a.read_metadata_from_json("metadata_nusselt.json", id="uid_000_table_metadata")

   a.read_metadata_from_json("metadata_peclet.json", id="uid_001_pe_metadata")
   a.read_metadata_from_json("metadata_nusselt.json", id="uid_002_nu_metadata")
   a.read_metadata_from_json("metadata_mid.json", id="uid_003_mid_metadata")

Now, you can assign a metadata object to a data object using the corresponding IDs:

.. code-block :: python

   a.assign_metadata("uid_000_table_metadata", "uid_000_table_data")

   a.assign_metadata("uid_001_pe_metadata", "uid_001_pe_data")
   a.assign_metadata("uid_002_nu_metadata", "uid_002_nu_data")
   a.assign_metadata("uid_003_mid_metadata", "uid_003_mid_data")


Exporting the FAIRified data
----------------------------

Once you have assigned all the necessary metadata objects to the corresponding data objects,
you can export the `Serializer` instance. Currently, only the export to HDF5 is supported:

.. code-block :: python

   a.to_hdf5("my_FAIR_table.hdf5", index_by="id")

In the near future, we will also implement the possibility to export to an RO-crate.