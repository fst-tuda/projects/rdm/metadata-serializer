.. FAIRtables documentation master file, created by
   sphinx-quickstart on Thu Nov 16 14:06:52 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FAIRtables documentation!
===================================================

.. toctree::
   :maxdepth: 2

   Installation
   Getting Started
   Command Line Tool
   autoapi/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
